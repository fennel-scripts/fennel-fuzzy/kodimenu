#!/bin/env fennel
(local fennel (require :fennel))
(set fennel.path (.. fennel.path ";/home/erik/fennelbin/lib/fzf/?.fnl"))

;;(local fzf (require :lib.fzf.fzf))
(local fzf (require :fzf))
(local borders fzf.borders)
(local presets fzf.presets)


(local cfg (fzf.show
			fzf.feeders.echo
			[
			 :lifx
			 :kodi
			 :gamepad
			 :lightsout
			 :kdeconnect
			 :screensoff
			 :loggoff
			 :refresh
			 ]
			(presets.selector
			 {:border borders.rounded
			  :bind "enter:execute(kodimenu {})"}
			 )
			)
	   )


(local action (or (. arg 1) "default"))

(fn run [cmd]
  (os.execute (.. cmd "&"))
  (os.execute (.. "notify-send 'info' '" cmd "'"))
  ;;(print cmd)
  )


(match action
  :lifx (run "lifx toggle")
  :kodi (run "kodi -fs")
  :gamepad (run "antimicrox --tray --profile $HOME/Desktop/kodiControls.gamecontroller.amgp ")
  :lightsout (run "openrgb --color 000002&ckb-next -m dark&lifx off")
  :kdeconnect (run "kdeconnect-app")
  :screensoff (run "xrandr --output DVI-D-0 --off --output HDMI-0 --off --output DP-0 --off --output DP-1 --primary --mode 1920x1080 --pos 0x0")
  :loggoff (run "neon-logout;sleep 10")
  :refresh (run "exit" true)
  :default	(print (. (fzf.capture cfg) :out))
  _ (print (. (fzf.capture cfg) :out))
  )
